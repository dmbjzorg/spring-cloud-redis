package com.dmbjz.point;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.dmbjz.point.dao")
public class PointApplication {

    public static void main(String[] args) {
        SpringApplication.run(PointApplication.class,args);
    }

}
