package com.dmbjz.seckill;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication
@MapperScan(basePackages = "com.dmbjz.seckill.dao")
@EnableOpenApi
public class SkillApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkillApplication.class,args);
    }

}
