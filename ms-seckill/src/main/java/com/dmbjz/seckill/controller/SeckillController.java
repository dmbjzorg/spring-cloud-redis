package com.dmbjz.seckill.controller;


import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.model.entity.SeckillVouchers;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.seckill.server.SeckillServcice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
@Api(tags = "代金券秒杀活动服务")
@RequestMapping("seckill")
public class SeckillController {

    @Autowired
    private SeckillServcice seckillServcice;
    @Autowired
    private HttpServletRequest request;

    @PostMapping("add")
    @ApiOperation("代金券抢购活动保存")
    public ResultInfo<String> addSeckillVouchers(@RequestBody SeckillVouchers seckillVouchers){

        seckillServcice.addSeckillVoucher(seckillVouchers);
        return ResultInfoUtil.buildSuccess(request.getServletPath(),"代金券抢购活动保存成功!");

    }

    @RequestMapping("buy/{voucherId}")
    public ResultInfo<String> doSeckill(@PathVariable("voucherId") Integer voucherId,String access_token){
        ResultInfo resultInfo = seckillServcice.doSeckill(voucherId,access_token,request.getServletPath());
        return resultInfo;

    }

}
