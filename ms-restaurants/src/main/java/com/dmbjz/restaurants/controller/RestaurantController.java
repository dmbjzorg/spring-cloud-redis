package com.dmbjz.restaurants.controller;

import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.model.entity.Restaurant;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.restaurants.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/*餐厅Controller*/
@RestController
@RequestMapping("/ct/restaurant")
public class RestaurantController {


    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private HttpServletRequest request;


    /*根据餐厅 ID 查询餐厅数据*/
    @GetMapping("/search/{restaurantId}")
    public ResultInfo<Restaurant> findById(@PathVariable Integer restaurantId) {
        Restaurant restaurant = restaurantService.findById(restaurantId);
        return ResultInfoUtil.buildSuccess(request.getServletPath(), restaurant);
    }


}
