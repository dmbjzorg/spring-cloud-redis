package com.dmbjz.oauth2.config;

import com.dmbjz.common.model.domain.SignInidentity;
import com.dmbjz.oauth2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import java.util.LinkedHashMap;

/*授权服务*/
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private RedisTokenStore redisTokenStore;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PasswordEncoder passwordEncoder;//密码加密器
    @Autowired
    private ClientOAuth2DataConfiguration configuration;//自定义配置类
    @Autowired
    private UserService userService;//登陆校验


    /*配置令牌安全管理约束*/
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {

       security.tokenKeyAccess("permitAll()") //允许访问Token的公钥，默认 /oauth/token_key是受保护的
               .checkTokenAccess("permitAll()");        //允许检查token的状态，默认 /oauth/token_key是受保护的

    }

    /*客户端配置，配置授权模型*/
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
       clients.inMemory().withClient(configuration.getClientId())
               .secret(passwordEncoder.encode(configuration.getSecret()))
               .refreshTokenValiditySeconds(configuration.getRefreshTokenValiditySeconds())
               .accessTokenValiditySeconds(configuration.getAccessTokenValiditySeconds())
               .scopes(configuration.getScope())
               .authorizedGrantTypes(configuration.getGrantType());//授权类型
    }

    /*配置访问端点和令牌服务*/
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager)
                .userDetailsService(userService)     //具体登录方法
                .tokenStore(redisTokenStore)       //存放Token的方式
                /*令牌增强对象*/
                .tokenEnhancer((accessToken,authentication)->{
                    SignInidentity principal = (SignInidentity) authentication.getPrincipal();//获取登录后用户的信息并设置
                    LinkedHashMap hashMap = new LinkedHashMap();
                    hashMap.put("nickname",principal.getNickname());
                    hashMap.put("avatarUrl",principal.getAvatarUrl());
                    DefaultOAuth2AccessToken auth2AccessToken = (DefaultOAuth2AccessToken) accessToken;
                    auth2AccessToken.setAdditionalInformation(hashMap);
                    return auth2AccessToken;
                });
    }


}
