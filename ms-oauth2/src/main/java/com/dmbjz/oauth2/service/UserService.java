package com.dmbjz.oauth2.service;

import com.dmbjz.common.model.domain.SignInidentity;
import com.dmbjz.common.model.entity.Diners;
import com.dmbjz.common.utils.AssertUtil;
import com.dmbjz.oauth2.dao.DinersDao;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/*SpringSecurity的用户登陆密码验证*/
@Service
public class UserService implements UserDetailsService {

    @Autowired
    private DinersDao dinersDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AssertUtil.isNotEmpty(username,"请输入用户名");
        Diners diners = dinersDao.selectByAccountInfo(username);
        if (diners == null) {
            throw new UsernameNotFoundException("用户名或密码错误!");
        }
        /*初始化登录认证对象*/
        SignInidentity signInidentity = new SignInidentity();
        BeanUtils.copyProperties(diners,signInidentity);
        return signInidentity;
    }


}
