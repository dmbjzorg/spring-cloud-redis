package com.dmbjz.feeds.controller;

import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.model.entity.Feeds;
import com.dmbjz.common.model.vo.FeedsVO;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.feeds.service.FeedsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("feeds")
public class FeedsController {

    @Autowired
    private FeedsService feedsService;
    @Autowired
    private HttpServletRequest request;

    /**
     * 添加 Feed
     *
     * @param feeds
     * @param access_token
     * @return
     */
    @PostMapping("addFeeds")
    public ResultInfo<String> create(@RequestBody Feeds feeds, String access_token) {
        feedsService.create(feeds, access_token);
        return ResultInfoUtil.buildSuccess(request.getServletPath(), "添加成功");
    }

    /*删除Feed*/
    @PostMapping("delFeeds")
    public ResultInfo delete(Integer id,String access_token){
        feedsService.deleteFeeds(id,access_token);
        return ResultInfoUtil.buildSuccess(request.getServletPath(), "删除成功");
    }



    /*变更Feed*/
    @PostMapping("updateFollowingFeeds/{followingDinerId}")
    public ResultInfo addFollowingFeeds(@PathVariable("followingDinerId") Integer followingDinerId, String access_token, Integer type){
        feedsService.addFollowingFeed(followingDinerId,access_token,type);
        return ResultInfoUtil.buildSuccess(request.getServletPath(), "操作成功");
    }

    /*分页获取关注的Feed数据*/
    @GetMapping("show/{page}")
    public ResultInfo selectForPage(@PathVariable Integer page,String access_token){
        List<FeedsVO> feedsVOS = feedsService.selectForPage(page, access_token);
        return ResultInfoUtil.buildSuccess(request.getServletPath(), feedsVOS);
    }

}
