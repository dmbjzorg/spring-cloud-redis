package com.dmbjz.feeds;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.dmbjz.feeds.dao")
public class FeedsApplicaiton {

    public static void main(String[] args) {
        SpringApplication.run(FeedsApplicaiton.class,args);
    }

}
