package com.dmbjz.follow.controller;

import com.dmbjz.common.constant.ApiConstant;
import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.follow.service.FollowService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

/*关注服务*/
@RestController
@RequestMapping("follow")
public class FollowController {

    @Autowired
    private FollowService followService;
    @Autowired
    private HttpServletRequest request;


    /*获取共同关注*/
    @GetMapping("commons/{dinerId}")
    public ResultInfo findCommonFriends(@PathVariable Integer dinerId,String access_token){
        return followService.findCommonsFriends(dinerId,access_token,request.getServletPath());
    }

    /**
     * 关注/取关
     *
     * @param followDinerId 关注的食客ID
     * @param isFollowed    是否关注 1=关注 0=取消
     * @param access_token  登录用户token
     * @return
     */
    @PostMapping("/{followDinerId}")
    public ResultInfo follow(@PathVariable Integer followDinerId,
                             @RequestParam int isFollowed,
                             String access_token) {
        ResultInfo resultInfo = followService.follow(followDinerId,
                isFollowed, access_token, request.getServletPath());
        return resultInfo;
    }


    /*传递用户ID，获取粉丝数据*/
    @GetMapping("followers/{dinerId}")
    public ResultInfo findFollowers(@PathVariable("dinerId") Integer dinerId){
        return ResultInfoUtil.build(ApiConstant.SUCCESS_CODE,"粉丝列表获取成功",request.getServletPath(),followService.findFollowers(dinerId));
    }


}
