package com.dmbjz.diners.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/*客戶端配置类*/
@Component
@ConfigurationProperties(prefix = "client.oauth2")
@Getter
@Setter
public class OAuth2ClintConfiguration {

    private String clientId;//客户端ID
    private String secret;
    private String grant_type;
    private String scope;

}
