package com.dmbjz.diners.controller;

import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.diners.service.SendVerifyCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/*验证码*/
@RestController
public class SendVerifyCodeController {

    @Autowired
    private SendVerifyCodeService sendVerifyCodeService;
    @Autowired
    private HttpServletRequest request;

    /*用户登录验证码发送*/
    @RequestMapping("send")
    public ResultInfo send(String phone){
        String sendCode = sendVerifyCodeService.send(phone);
        return ResultInfoUtil.buildSuccess("验证码:"+sendCode,request.getServletPath());
    }


}
