package com.dmbjz.diners.controller;

import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.diners.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("sign")
public class SignController {

    @Autowired
    private SignService signService;
    @Autowired
    private HttpServletRequest request;

    /*用户签到并返回连续签到次数*/
    @PostMapping("signing")
    public ResultInfo signInfo(String accessToken,@RequestParam(required = false) String dateStr){
        int signCount = signService.doSign(accessToken, dateStr);
        return ResultInfoUtil.buildSuccess(request.getServletPath(),signCount);
    }

    /*返回用户签到次数*/
    @PostMapping("signCount")
    public ResultInfo getSignCount(String accessToken,@RequestParam(required = false) String dateStr){
        Long signCount = signService.getUserSignCount(accessToken, dateStr);
        return ResultInfoUtil.buildSuccess(request.getServletPath(),signCount);
    }

    /*返回用户签到具体信息*/
    @PostMapping("signMap")
    public ResultInfo getSignMap(String accessToken,@RequestParam(required = false) String dateStr){
        Map<String, Boolean> signInfo = signService.getSignInfo(accessToken, dateStr);
        return ResultInfoUtil.buildSuccess(request.getServletPath(),signInfo);
    }


}
