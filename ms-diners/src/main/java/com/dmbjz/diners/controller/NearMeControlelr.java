package com.dmbjz.diners.controller;

import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.model.vo.NearMeDinerVO;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.diners.service.NearMeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/*附近的人*/
@RestController
@RequestMapping(path = "nearname")
public class NearMeControlelr {


    @Autowired
    private NearMeService nearMeService;
    @Autowired
    private HttpServletRequest request;

    /*保存用户的地理位置 */
    @RequestMapping("save")
    public ResultInfo updateDinerLocation(String accessToken,Float lon,Float lat){

        nearMeService.updateDinerLocation(accessToken,lon,lat);
        return ResultInfoUtil.buildSuccess("地理位置保存成功!",request.getServletPath());

    }


    /*获取附近的人*/
    @GetMapping("search")
    public ResultInfo nearMe(String access_token, Integer radius, Float lon, Float lat) {

        List<NearMeDinerVO> nearMe = nearMeService.findNearMe(access_token, radius, lon, lat);
        return ResultInfoUtil.buildSuccess(request.getServletPath(), nearMe);

    }

}
