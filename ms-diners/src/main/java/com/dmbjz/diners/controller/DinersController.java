package com.dmbjz.diners.controller;

import com.dmbjz.common.model.domain.ResultInfo;
import com.dmbjz.common.model.dto.DinersDTO;
import com.dmbjz.common.model.vo.ShortDinerInfo;
import com.dmbjz.common.utils.ResultInfoUtil;
import com.dmbjz.diners.service.DinersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "用户相关接口")
public class DinersController {

    @Autowired
    private DinersService dinersService;

    @Autowired
    private HttpServletRequest request;

    /*根据用户ID集合查询用户信息*/
    @GetMapping("findByIds")
    public ResultInfo<List<ShortDinerInfo>> findByIds(String ids) {
        List<ShortDinerInfo> dinerInfos = dinersService.findByIds(ids);
        return ResultInfoUtil.buildSuccess(request.getServletPath(), dinerInfos);
    }

    /*登录*/
    @PostMapping("login")
    @ApiOperation(value = "用户登录接口")
    public ResultInfo signIn(String account,String password){
        return dinersService.signIn(account,password,request.getServletPath());
    }

    /*校验手机号是否已注册*/
    @PostMapping("checkPhone")
    @ApiOperation(value = "手机号验证接口")
    public ResultInfo checkPhone(String phone){
        dinersService.checkPhoneIsRegistered(phone);
        return ResultInfoUtil.buildSuccess(request.getServletPath());
    }

    /*用户注册*/
    @RequestMapping("register")
    public ResultInfo register(@RequestBody DinersDTO dinersDTO){
        return dinersService.register(dinersDTO,request.getServletPath());
    }


}
